

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sys_user_role_role_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[sys_user_role]'))
ALTER TABLE [dbo].[sys_user_role] DROP CONSTRAINT [FK_sys_user_role_role_id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sys_user_role_user_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[sys_user_role]'))
ALTER TABLE [dbo].[sys_user_role] DROP CONSTRAINT [FK_sys_user_role_user_id]
GO



/****** Object:  Table [dbo].[sys_user_role]    Script Date: 03/03/2021 10:52:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sys_user_role]') AND type in (N'U'))
DROP TABLE [dbo].[sys_user_role]
GO


/****** Object:  Table [dbo].[sys_user_role]    Script Date: 03/03/2021 10:52:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[sys_user_role](
	[id] [varchar](30) NOT NULL,
	[user_id] [varchar](30) NOT NULL,
	[role_id] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SYS_USER_ROLE_ID] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[user_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�û�ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_role', @level2type=N'COLUMN',@level2name=N'user_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��ɫID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_role', @level2type=N'COLUMN',@level2name=N'role_id'
GO

ALTER TABLE [dbo].[sys_user_role]  WITH NOCHECK ADD  CONSTRAINT [FK_sys_user_role_role_id] FOREIGN KEY([role_id])
REFERENCES [dbo].[sys_role] ([id])
GO

ALTER TABLE [dbo].[sys_user_role] NOCHECK CONSTRAINT [FK_sys_user_role_role_id]
GO

ALTER TABLE [dbo].[sys_user_role]  WITH CHECK ADD  CONSTRAINT [FK_sys_user_role_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[sys_user] ([id])
GO

ALTER TABLE [dbo].[sys_user_role] CHECK CONSTRAINT [FK_sys_user_role_user_id]
GO


