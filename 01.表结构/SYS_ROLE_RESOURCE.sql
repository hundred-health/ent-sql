

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sys_role_resource_resource_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[sys_role_resource]'))
ALTER TABLE [dbo].[sys_role_resource] DROP CONSTRAINT [FK_sys_role_resource_resource_id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sys_role_resource_role_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[sys_role_resource]'))
ALTER TABLE [dbo].[sys_role_resource] DROP CONSTRAINT [FK_sys_role_resource_role_id]
GO



/****** Object:  Table [dbo].[sys_role_resource]    Script Date: 03/03/2021 10:44:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sys_role_resource]') AND type in (N'U'))
DROP TABLE [dbo].[sys_role_resource]
GO



/****** Object:  Table [dbo].[sys_role_resource]    Script Date: 03/03/2021 10:44:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sys_role_resource](
	[id] [varchar](30) NOT NULL,
	[role_id] [varchar](30) NOT NULL,
	[resource_id] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SYS_ROLE_RESOURCE_ID] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[role_id] ASC,
	[resource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sys_role_resource]  WITH CHECK ADD  CONSTRAINT [FK_sys_role_resource_resource_id] FOREIGN KEY([resource_id])
REFERENCES [dbo].[sys_resource] ([id])
GO

ALTER TABLE [dbo].[sys_role_resource] CHECK CONSTRAINT [FK_sys_role_resource_resource_id]
GO

ALTER TABLE [dbo].[sys_role_resource]  WITH CHECK ADD  CONSTRAINT [FK_sys_role_resource_role_id] FOREIGN KEY([role_id])
REFERENCES [dbo].[sys_role] ([id])
GO

ALTER TABLE [dbo].[sys_role_resource] CHECK CONSTRAINT [FK_sys_role_resource_role_id]
GO


