﻿

/****** Object:  Table [dbo].[sys_log]    Script Date: 03/03/2021 10:37:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sys_log]') AND type in (N'U'))
DROP TABLE [dbo].[sys_log]
GO


/****** Object:  Table [dbo].[sys_log]    Script Date: 03/03/2021 10:37:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sys_log](
	[id] [varchar](30) NOT NULL,
	[account] [varchar](20) NULL,
	[user_id] [varchar](30) NULL,
	[ip] [varchar](20) NULL,
	[ajax] [int] NULL,
	[uri] [varchar](250) NULL,
	[params] [varchar](2000) NULL,
	[http_method] [varchar](20) NULL,
	[class_method] [varchar](100) NULL,
	[action_name] [varchar](30) NULL,
	[create_date] [datetime] NULL,
 CONSTRAINT [PK_SYS_LOG_ID] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'user_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调用方IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'ip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ajax请求类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'ajax'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求的url相对地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'uri'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入参' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'params'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GET/POST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'http_method'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类方法名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'class_method'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接口说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'action_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'create_date'
GO


