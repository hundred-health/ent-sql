
/****** Object:  Table [dbo].[SYS_DICT]    Script Date: 03/03/2021 10:34:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SYS_DICT]') AND type in (N'U'))
DROP TABLE [dbo].[SYS_DICT]
GO


/****** Object:  Table [dbo].[SYS_DICT]    Script Date: 03/03/2021 10:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sys_dict](
	[id] [varchar](64) NOT NULL,
	[item_value] [varchar](100) NOT NULL,
	[item_name] [varchar](100) NOT NULL,
	[class_code] [varchar](100) NOT NULL,
	[simple_spelling] [varchar](50) NULL,
	[simple_wubi] [varchar](50) NULL,
	[sort] [int] NOT NULL,
	[remarks] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'item_value'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标签名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'item_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'class_code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'五笔码首拼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'simple_spelling'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'五笔码首拼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'simple_wubi'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序（升序）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'sort'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICT', @level2type=N'COLUMN',@level2name=N'remarks'
GO


