﻿

/****** Object:  Table [dbo].[sys_resource]    Script Date: 03/03/2021 10:40:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sys_resource]') AND type in (N'U'))
DROP TABLE [dbo].[sys_resource]
GO



/****** Object:  Table [dbo].[sys_resource]    Script Date: 03/03/2021 10:40:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sys_resource](
	[id] [varchar](30) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[parent_id] [varchar](30) NULL,
	[type] [smallint] NOT NULL,
	[url] [varchar](100) NULL,
	[permission] [varchar](50) NULL,
	[color] [varchar](10) NULL,
	[icon] [varchar](50) NULL,
	[system_id] [varchar](20) NULL,
	[sort] [int] NOT NULL,
	[verification] [int] NOT NULL,
	[create_date] [datetime] NOT NULL,
	[simple_spelling] [varchar](20) NULL,
 CONSTRAINT [PK_SYS_RESOURCE_ID] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'资源名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父节点id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'parent_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型   0：目录   1：菜单   2：按钮' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'url地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'url'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'权限标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'permission'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'color'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'icon'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'sort'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_resource', @level2type=N'COLUMN',@level2name=N'create_date'
GO

ALTER TABLE [dbo].[sys_resource] ADD  CONSTRAINT [DF__sys_resour__sort__239E4DCF]  DEFAULT ('0') FOR [sort]
GO

ALTER TABLE [dbo].[sys_resource] ADD  CONSTRAINT [DF__sys_resou__verif__24927208]  DEFAULT ('0') FOR [verification]
GO


