
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('01','1','省市级','1001001','ssj','iyx','1','')
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('5','2','地市级','1001001','dsj','fyx','2',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('6','3','区县级','1001001','qxj','aex','3',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('7','4','街道(乡镇)级','1001001','jd(xz)j','tu(xq)x','4',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('8','5','居委(村)级','1001001','jw(c)j','nt(s)x','5',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('9','01','社区服务中心(卫生院)','1001002','sqfwzx(wsy)','paetkn(btb)','1','')
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('10','02','社区服务站(卫生室)','1001002','sqfwz(wss)','paetu(btp)','2',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('11','03','卫健委','1001002','wjw','bwt','3',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('12','04','疾控中心','1001002','jkzx','urkn','4',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('13','05','妇幼保健所','1001002','fybjs','vxwwr','5',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('14','06','妇幼保健院','1001002','fybjy','vxwwb','6',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('15','07','医院','1001002','yy','ab','7',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('16','08','健康教育所(中心、站)','1001002','jkjys(zx、z)','wyfyr(kn、u)','8',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('17','09','急救中心(站)','1001002','jjzx(z)','qfkn(u)','9',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('18','10','专科疾病防治院(所、站)','1001002','zkjbfzy(s、z)','ftuubib(r、u)','10',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('19','11','卫生监督局(所)','1001002','wsjdj(s)','btjhn(r)','11',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('20','12','其他卫生机构','1001002','qtwsjg','awbtss','12',NULL)
;


insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('21','01','科室一','1001003','ksy','','1','')
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('22','02','科室二','1001003','kse','','2',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('23','03','科室三','1001003','kss','','3',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('24','01','主治医师','1001004','zzys','','1','')
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('25','02','副主任医师','1001004','fzrys','','2',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('26','03','主任医师','1001004','zrys','','3',NULL)
;
insert into SYS_DICT(id,item_value,item_name,class_code,simple_spelling,simple_wubi,sort,remarks)
values('27','04','护士','1001004','hs','','4',NULL)
;