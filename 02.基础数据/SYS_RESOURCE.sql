
insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('1', '用户管理', '0', 0, null, null, null, 'iconfont iconmenu-management', null, 1, 1, GETDATE(), null);

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('2', '用户列表', '1', 1, '/system/user', 'system:user', null, 'iconfont iconmenu-management', null, 2, 1, GETDATE(), null);

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('989417091599802370', '用户列表', '987981486382686210', 2, '/system/user/list', 'system:user:list', '#19BE6B', null, null, 3, 1, GETDATE(), 'yhlb');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('987986018126831617', '用户添加', '987981486382686210', 2, '/system/user/add', 'system:user:add', '#19BE6B', null, null, 4, 1, GETDATE(), 'yhtj');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('987986318946508801', '用户更新', '987981486382686210', 2, '/system/user/update', 'system:user:update', '#19BE6B', null, null, 5, 1, GETDATE(), 'yhgx');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('987986542024761345', '用户删除', '987981486382686210', 2, '/system/user/remove', 'system:user:remove', '#ED3F14', null, null, 6, 1, GETDATE(), 'yhsc');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('989417919966453762', '密码重置', '987981486382686210', 2, '/system/user/resetPassword', 'system:user:resetPassword', '#19BE6B', null, null, 7, 1, GETDATE(), 'mmzz');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('989418114355666946', '锁定用户', '987981486382686210', 2, '/system/user/lock', 'system:user:lock', '#FF9900', null, null, 8, 1, GETDATE(), 'sdyh');

insert into sys_resource (ID, NAME, PARENT_ID, TYPE, URL, PERMISSION, COLOR, ICON, SYSTEM_ID, SORT, VERIFICATION, CREATE_DATE, SIMPLE_SPELLING)
values ('989418202087923713', '解锁用户', '987981486382686210', 2, '/system/user/unlock', 'system:user:unlock', '#FF9900', null, null, 9, 1, GETDATE(), 'jsyh');

